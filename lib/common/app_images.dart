class AppImages {
  static const icLogo = "assets/images/ic_logo.png";
  static const icMenu = "assets/images/ic_menu.png";
  static const icSearch = "assets/images/ic_search.png";
  static const icShoppingBag = "assets/images/ic_shopping_bag.png";
  static const icBanner = "assets/images/ic_banner.png";
  static const icCollection = "assets/images/ic_collections.png";
  static const icItem1 = "assets/images/ic_item_1.png";
  static const icItem2 = "assets/images/ic_item_2.png";
  static const icItem3 = "assets/images/ic_item_3.png";
  static const icItem4 = "assets/images/ic_item_4.png";
  static const icNewArrival = "assets/images/ic_new_arrival.png";
  static const icArrowRight = "assets/images/ic_arrow_right.png";
}
