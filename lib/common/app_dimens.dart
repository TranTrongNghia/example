import 'package:get/get.dart';

class AppDimens {
  static double appbarIconSize = Get.width * 24 / 375;
  static double appbarSpacer = Get.width * 16 / 375;
}