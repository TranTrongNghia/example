import 'package:example/common/app_images.dart';
import 'package:flutter/material.dart';

class CollectionAppBar extends AppBar {
  CollectionAppBar() : super (
      title: Row(
          children: [
            Expanded(child: Image.asset(AppImages.icCollection, width: 177, height: 40)),
          ]
      )
  ); // thieu dau ;
}
