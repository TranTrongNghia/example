import 'package:flutter/material.dart';

import 'appbar/collection_appbar.dart';

class CollectionWidget extends StatelessWidget {
  const CollectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CollectionAppBar(),
        body: const Center(child: Text("body"))
    );
  }

}
  