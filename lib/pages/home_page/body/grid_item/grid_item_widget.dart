import 'package:example/common/app_images.dart';
import 'package:example/pages/home_page/body/grid_item/entity/grid_item_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GridItemWidget extends StatelessWidget {
  GridItemWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildTitle(),
        const SizedBox(height: 6),
        _buildTabbar(),
        const SizedBox(height: 23),
        _buildGridItems(),
        const SizedBox(height: 40),
        _buildExploreMore(),
        const SizedBox(height: 40),
      ],
    );
  }

  Widget _buildTitle() {
    return AspectRatio(
      aspectRatio: 375 / 50,
      child: Image.asset(AppImages.icNewArrival, width: Get.width),
    );
  }

  List<String> tabTitles = ["All", "Apparel", "Dress", "Tshirt", "Bag"];

  Widget _buildTabbar() {
    return Wrap(
      direction: Axis.horizontal,
      children: tabTitles.asMap().entries.map((e) {
        String title = e.value;
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 13),
          child: Text(
            title,
            style: const TextStyle(
              fontFamily: "TenorSans",
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: Color(0xff888888),
            ),
          ),
        );
      }).toList(),
    );
  }

  List<GridItemEntity> tabItems = [
    GridItemEntity(
        image: AppImages.icItem1,
        name: "21WN reversible angora cardigan",
        price: 120),
    GridItemEntity(
        image: AppImages.icItem4,
        name: "21WN reversible angora cardigan",
        price: 120),
    GridItemEntity(
        image: AppImages.icItem2,
        name: "21WN reversible angora cardigan",
        price: 120),
    GridItemEntity(image: AppImages.icItem3, name: "Oblong bag", price: 120),
  ];

  Widget _buildGridItems() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      height: Get.width * 532 / 375,
      child: GridView.count(
        physics: const NeverScrollableScrollPhysics(),
        crossAxisSpacing: 13,
        mainAxisSpacing: 13,
        childAspectRatio: 165 / 260,
        crossAxisCount: 2,
        children: tabItems.asMap().entries.map((e) {
          GridItemEntity element = e.value;
          return Column(
            children: [
              AspectRatio(
                aspectRatio: 165 / 200,
                child: Image.asset(
                  element.image,
                ),
              ),
              const SizedBox(height: 11),
              Text(
                element.name,
                style: const TextStyle(
                    fontFamily: "TenorSans",
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff333333)),
                textAlign: TextAlign.center,
              ),
              Text(
                "\$${element.price}",
                style: const TextStyle(
                    fontFamily: "TenorSans",
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    color: Color(0xffDD8560)),
              )
            ],
          );
        }).toList(),
      ),
    );
  }

  Widget _buildExploreMore() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "Explore More",
          style: TextStyle(
              fontFamily: "TenorSans",
              fontSize: 16,
              fontWeight: FontWeight.w400,
              color: Color(0xff000000),
          ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(width: 8),
        Image.asset(AppImages.icArrowRight, width: 18, height: 18)
      ],
    );
  }
}
