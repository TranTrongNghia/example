class GridItemEntity {
  String image;
  String name;
  double price;

  GridItemEntity({required this.image, required this.name, required this.price});
}