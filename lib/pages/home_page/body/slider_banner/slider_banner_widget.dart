import 'package:example/common/app_images.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SliderBannerWidget extends StatelessWidget {
  const SliderBannerWidget({Key? key}) : super(key: key);
 // gan vao do phai là 1 statelessWidget như nay
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AspectRatio(
            aspectRatio: 375 / 600,
            child: Image.asset(AppImages.icBanner, width: Get.width)),
        Positioned.fill(
          bottom: 40,
          child: Align(
            alignment: Alignment.bottomCenter,
            child: _buildButton(),
          ),
        ),
      ],
    );
  }

  Widget _buildButton() {
    return Container(
      padding:  EdgeInsets.symmetric(horizontal: 30, vertical: 8),
      decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.4),
          borderRadius: BorderRadius.circular(30)),
      child:  const Text(
        "EXPLORE COLLECTION",
        style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            fontFamily: 'TenorSans',
            color: Colors.white),
      ),
    );
  }
}
