import 'package:example/common/app_dimens.dart';
import 'package:example/common/app_images.dart';
import 'package:flutter/material.dart';

class HPAppBar extends AppBar{
  HPAppBar({Key? key}): super (key: key,
    title: Row(
      children: [
        Image.asset(AppImages.icMenu, width: AppDimens.appbarIconSize, height: AppDimens.appbarIconSize),
        SizedBox(width: AppDimens.appbarSpacer),
        Container(width: AppDimens.appbarIconSize, height: AppDimens.appbarIconSize, color: Colors.white),
        Expanded(child: Image.asset(AppImages.icLogo, width: AppDimens.appbarIconSize, height: AppDimens.appbarIconSize)),
        Image.asset(AppImages.icSearch, width: AppDimens.appbarIconSize, height: AppDimens.appbarIconSize),
        SizedBox(width: AppDimens.appbarSpacer),
        Image.asset(AppImages.icShoppingBag, width: AppDimens.appbarIconSize, height: AppDimens.appbarIconSize),
      ],
    )
  );
}