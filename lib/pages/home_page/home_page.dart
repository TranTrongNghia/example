import 'package:flutter/material.dart';

import 'appbar/hp_appbar.dart';
import 'body/grid_item/grid_item_widget.dart';
import 'body/slider_banner/slider_banner_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HPAppBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SliderBannerWidget(),
            const SizedBox(height: 27),
            GridItemWidget(),
            //CollectionWidget() // ? sao lai la collection appbar? sai!
          ],
        ),
      ),
    );
  }
}
